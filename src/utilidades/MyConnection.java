package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MyConnection {
    public String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    public String user = "admin";
    public String pass = "admin";
    public String ip = "192.168.56.101";
    public String puerto = "3306";
    public String database_name = "f1";
    public String params = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";

    public MyConnection(String JDBC_DRIVER, String user, String pass, String ip, String puerto, String database_name, String params) {
        this.JDBC_DRIVER = JDBC_DRIVER;
        this.user = user;
        this.pass = pass;
        this.ip = ip;
        this.puerto = puerto;
        this.database_name = database_name;
        this.params = params;
    }

    public MyConnection(String user, String pass, String database_name) {
        this.user = user;
        this.pass = pass;
        this.database_name = database_name;
    }

    public MyConnection(String database_name) {
        this.database_name = database_name;
    }

    public MyConnection(){}

    public Connection getConnection(){
        Connection connection = null;
        PreparedStatement stmt = null;
        String url = "jdbc:mysql://"+this.ip+":"+this.puerto+"/"+this.database_name+this.params;
        try {
            connection = DriverManager.getConnection(url,this.user,this.pass);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;
    }
}

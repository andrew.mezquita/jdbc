package modelo;

import java.util.ArrayList;

public interface CircuitsDAO {
    //a.
    ArrayList<Circuits> select_all();
    //b.
    Circuits select_by_nombre(String nombre);
    //c.
    double select_avg_km();
    //d.
    boolean insert_circuit(Circuits cicuit);
    //e.
    boolean delete_circuit_by_nombre(String nombre);
    //f.
    boolean update_by_nombre(String nombre, String country, int km, String race_time);
    //g.
    boolean update_km_by_nombre(String nombre, int km);
}

package modelo;

public class Teams {
    private String nombre;
    private  String country;
    private String director;
    private String color;
    private String power_unit;
    private int title;

    public Teams(String nombre, String country, String director, String color, String power_unit, int title) {
        this.nombre = nombre;
        this.country = country;
        this.director = director;
        this.color = color;
        this.power_unit = power_unit;
        this.title = title;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPower_unit() {
        return power_unit;
    }

    public void setPower_unit(String power_unit) {
        this.power_unit = power_unit;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "teams{" +
                "nombre='" + nombre + '\'' +
                ", country='" + country + '\'' +
                ", director='" + director + '\'' +
                ", color='" + color + '\'' +
                ", power_unit='" + power_unit + '\'' +
                ", title=" + title +
                '}';
    }
}

package modelo;

import java.util.ArrayList;

public interface TeamsDAO {
    //a.
    ArrayList<Teams> select_all();
    //b.
    Teams select_by_nombre(String nombre);
    //c.
    double select_avg_titles();
    //d.
    boolean insert_team(Teams team);
    //e.
    boolean delete_team_by_nombre(String nombre);
    //f.
    boolean update_by_nombre(String nombre, String country, String director, String color, String power_unit, int titles);
    //g.
    boolean update_titles_by_nombre(String nombre, int titles);
}

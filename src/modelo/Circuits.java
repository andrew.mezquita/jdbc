package modelo;

public class Circuits {
    private String nombre;
    private String country;
    private int km;
    private String race_time;

    public Circuits(String nombre, String country, int km, String race_time) {
        this.nombre = nombre;
        this.country = country;
        this.km = km;
        this.race_time = race_time;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public String getRace_time() {
        return race_time;
    }

    public void setRace_time(String race_time) {
        this.race_time = race_time;
    }

    @Override
    public String toString() {
        return "Circuits{" +
                "nombre='" + nombre + '\'' +
                ", country='" + country + '\'' +
                ", km=" + km +
                ", race_time='" + race_time + '\'' +
                '}';
    }
}

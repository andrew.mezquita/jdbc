package modelo;

public class Drivers {
    private int num;
    private String nombre;
    private String surname;
    private int age;
    private int championships;
    private int victories;
    private int podiums;

    public Drivers(int number, String name, String surname, int age, int championships, int victories, int podiums) {
        this.num = number;
        this.nombre = name;
        this.surname = surname;
        this.age = age;
        this.championships = championships;
        this.victories = victories;
        this.podiums = podiums;
    }

    public int getNumber() {
        return num;
    }

    public void setNumber(int number) {
        this.num = number;
    }

    public String getName() {
        return nombre;
    }

    public void setName(String name) {
        this.nombre = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getChampionships() {
        return championships;
    }

    public void setChampionships(int championships) {
        this.championships = championships;
    }

    public int getVictories() {
        return victories;
    }

    public void setVictories(int victories) {
        this.victories = victories;
    }

    public int getPodiums() {
        return podiums;
    }

    public void setPodiums(int podiums) {
        this.podiums = podiums;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "number=" + num +
                ", name='" + nombre + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", championships=" + championships +
                ", victories=" + victories +
                ", podiums=" + podiums +
                '}';
    }
}

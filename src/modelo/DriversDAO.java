package modelo;

import java.util.ArrayList;

public interface DriversDAO {
    //a.
    ArrayList<Drivers> select_all();
    //b.
    Drivers select_by_num(int num);
    //c.
    double select_avg_age();
    //d.
    boolean insert_driver(Drivers driver);
    //e.
    boolean delete_driver_by_num(int num);
    //f.
    boolean update_by_num(int num, String nombre, String surname, int age, int championships, int victories, int podiums);
    //g.
    boolean update_championships_by_num(int num, int championships);
}

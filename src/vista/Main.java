package vista;

import controlador.CircuitsJDBC;
import controlador.DriversJDBC;
import controlador.TeamsJDBC;
import modelo.Circuits;
import modelo.Drivers;
import modelo.Teams;
import utilidades.MyConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Welcome to the F1 Database");

        MyConnection myconnection = new MyConnection();
        Connection connection = null;
        PreparedStatement stmt = null;

        try{
            System.out.println("Connecting to database...");
            connection = myconnection.getConnection();

            Scanner scN = new Scanner(System.in);
            Scanner scC = new Scanner(System.in);
            int option;

            do {
                menu();
                option = scN.nextInt();
                switch (option) {
                    case 1:
                        DriversJDBC driversController = new DriversJDBC(connection);
                        driversMenu();
                        int driversOption = scN.nextInt();
                        switch (driversOption) {
                            case 1:
                                System.out.println("SELECT ALL");
                                ArrayList<Drivers> lista = driversController.select_all();
                                for (Drivers driver:lista){
                                    System.out.println(driver);
                                }
                                break;
                            case 2:
                                System.out.println("SELECT BY NUM");
                                System.out.println("Choose Driver's Number");
                                int driverNum = scN.nextInt();
                                Drivers driver = driversController.select_by_num(driverNum);
                                System.out.println(driver);
                                break;
                            case 3:
                                System.out.println("AVG AGE");
                                double avg = driversController.select_avg_age();
                                System.out.println(avg);
                                break;
                            case 4:
                                System.out.println("INSERT DRIVER");
                                System.out.println("Choose Driver's Number");
                                int newDriverNum = scN.nextInt();
                                System.out.println("Choose Driver's Name");
                                String newDriverName = scC.nextLine();
                                System.out.println("Choose Driver's Surname");
                                String newDriverSurname = scC.nextLine();
                                System.out.println("Choose Driver's Age");
                                int newDriverAge = scN.nextInt();
                                System.out.println("Choose Driver's Championships");
                                int newDriverChampionships = scN.nextInt();
                                System.out.println("Choose Driver's Victories");
                                int newDriverVictories = scN.nextInt();
                                System.out.println("Choose Driver's Podiums");
                                int newDriverPodiums = scN.nextInt();
                                Drivers newDriver = new Drivers(newDriverNum, newDriverName, newDriverSurname, newDriverAge,newDriverChampionships,newDriverVictories,newDriverPodiums);
                                boolean insertResult = driversController.insert_driver(newDriver);
                                System.out.println("Driver inserted: " + insertResult);
                                break;
                            case 5:
                                System.out.println("DELETE DRIVER");
                                System.out.println("Choose Driver's Number");
                                int driverNumToDelete = scN.nextInt();
                                boolean deleteResult = driversController.delete_driver_by_num(driverNumToDelete);
                                System.out.println("Team deleted: " + deleteResult);
                                break;
                            case 6:
                                System.out.println("UPDATE DRIVER");
                                System.out.println("Choose Driver's Number");
                                int driverNumToUpdate = scN.nextInt();
                                System.out.println("Choose Driver's Name");
                                String newName = scC.nextLine();
                                System.out.println("Choose Driver's Surname");
                                String newSurname = scC.nextLine();
                                System.out.println("Choose Driver's Age");
                                int newAge = scN.nextInt();
                                System.out.println("Choose Driver's Championships");
                                int newChampionships = scN.nextInt();
                                System.out.println("Choose Driver's Victories");
                                int newVictories = scN.nextInt();
                                System.out.println("Choose Driver's Podiums");
                                int newPodiums = scN.nextInt();
                                boolean updateResult = driversController.update_by_num(driverNumToUpdate, newName, newSurname, newAge, newChampionships, newVictories, newPodiums);
                                System.out.println("Driver updated: " + updateResult);
                                break;
                            case 7:
                                System.out.println("UPDATE CHAMPIONSHIPS");
                                System.out.println("Choose Driver's Number");
                                int newDriverNumToUpdate = scN.nextInt();
                                System.out.println("Choose Driver's Championships");
                                int newDriverChampionshipsToUpdate = scN.nextInt();
                                boolean updateResultChampionships = driversController.update_championships_by_num(newDriverNumToUpdate, newDriverChampionshipsToUpdate);
                                System.out.println("Championships updated: " + updateResultChampionships);
                                break;
                            case 8:
                                break;
                            default:
                                System.out.println("Non valid option");
                                break;
                        }
                        break;
                    case 2:
                        TeamsJDBC teamsController = new TeamsJDBC(connection);
                        teamsMenu();
                        int teamsOption = scN.nextInt();
                        switch (teamsOption) {
                            case 1:
                                System.out.println("SELECT ALL");
                                ArrayList<Teams> lista = teamsController.select_all();
                                for (Teams team:lista){
                                    System.out.println(team);
                                }
                                break;
                            case 2:
                                System.out.println("SELECT BY NOMBRE");
                                System.out.println("Choose Team's Name");
                                String teamName = scC.nextLine();
                                Teams team = teamsController.select_by_nombre(teamName);
                                System.out.println(team);
                                break;
                            case 3:
                                System.out.println("AVG TITLES");
                                double avg = teamsController.select_avg_titles();
                                System.out.println(avg);
                                break;
                            case 4:
                                System.out.println("INSERT TEAM");
                                System.out.println("Choose Team's Name");
                                String newTeamName = scC.nextLine();
                                System.out.println("Choose Team's Country");
                                String newTeamCountry = scC.nextLine();
                                System.out.println("Choose Team's Director");
                                String newTeamDirector = scC.nextLine();
                                System.out.println("Choose Team's Color");
                                String newTeamColor = scC.nextLine();
                                System.out.println("Choose Team's Power Unit");
                                String newTeamPowerUnit = scC.nextLine();
                                System.out.println("Choose Team's Titles");
                                int newTeamTitles = scC.nextInt();
                                Teams newTeam = new Teams(newTeamName,newTeamCountry,newTeamDirector,newTeamColor,newTeamPowerUnit,newTeamTitles);
                                boolean insertResult = teamsController.insert_team(newTeam);
                                System.out.println("Team inserted: " + insertResult);
                                break;
                            case 5:
                                System.out.println("DELETE TEAM");
                                System.out.println("Choose Team's Name");
                                String teamNameToDelete = scC.nextLine();
                                boolean deleteResult = teamsController.delete_team_by_nombre(teamNameToDelete);
                                System.out.println("Team deleted: " + deleteResult);
                                break;
                            case 6:
                                System.out.println("UPDATE TEAM");
                                System.out.println("Choose Team's Name");
                                String teamNameToUpdate = scC.nextLine();
                                System.out.println("Choose Team's Country");
                                String newCountry = scC.nextLine();
                                System.out.println("Choose Team's Director");
                                String newDirector = scC.nextLine();
                                System.out.println("Choose Team's Color");
                                String newColor = scC.nextLine();
                                System.out.println("Choose Team's Power Unit");
                                String newPowerUnit = scC.nextLine();
                                System.out.println("Choose Team's Titles");
                                int newTitles = scN.nextInt();
                                boolean updateResult = teamsController.update_by_nombre(teamNameToUpdate, newCountry, newDirector, newColor, newPowerUnit, newTitles);
                                System.out.println("Team updated: " + updateResult);
                                break;
                            case 7:
                                System.out.println("UPDATE TITLES");
                                System.out.println("Choose Team's Name");
                                String newTeamNameToUpdate = scC.nextLine();
                                System.out.println("Choose Team's Titles");
                                int newTeamTitleToUpdate = scN.nextInt();
                                boolean updateResultTitles = teamsController.update_titles_by_nombre(newTeamNameToUpdate, newTeamTitleToUpdate);
                                System.out.println("Titles updated: " + updateResultTitles);
                                break;
                            case 8:
                                break;
                            default:
                                System.out.println("Non valid option");
                                break;
                        }
                        break;
                    case 3:
                        CircuitsJDBC circuitsController = new CircuitsJDBC(connection);
                        circuitsMenu();
                        int circuitsOption = scN.nextInt();
                        switch (circuitsOption) {
                            case 1:
                                System.out.println("SELECT ALL");
                                ArrayList<Circuits> lista = circuitsController.select_all();
                                for (Circuits circuit:lista){
                                    System.out.println(circuit);
                                }
                                break;
                            case 2:
                                System.out.println("SELECT BY NOMBRE");
                                System.out.println("Choose Circuit's Name");
                                String circuitName = scC.nextLine();
                                Circuits circuit = circuitsController.select_by_nombre(circuitName);
                                System.out.println(circuit);
                                break;
                            case 3:
                                System.out.println("AVG KM");
                                double avg = circuitsController.select_avg_km();
                                System.out.println(avg);
                                break;
                            case 4:
                                System.out.println("INSERT CIRCUIT");
                                System.out.println("Choose Circuit's Name");
                                String newCircuitName = scC.nextLine();
                                System.out.println("Choose Circuit's Country");
                                String newCircuitCountry = scC.nextLine();
                                System.out.println("Choose Circuit's Km");
                                int newCircuitKm = scN.nextInt();
                                System.out.println("Choose Circuit's Race Time");
                                String newCircuitRaceTime = scC.nextLine();
                                Circuits newCircuit = new Circuits(newCircuitName,newCircuitCountry,newCircuitKm,newCircuitRaceTime);
                                boolean insertResult = circuitsController.insert_circuit(newCircuit);
                                System.out.println("Circuit inserted: " + insertResult);
                                break;
                            case 5:
                                System.out.println("DELETE CIRCUIT");
                                System.out.println("Choose Circuit's Name");
                                String circuitNameToDelete = scC.nextLine();
                                boolean deleteResult = circuitsController.delete_circuit_by_nombre(circuitNameToDelete);
                                System.out.println("Circuit deleted: " + deleteResult);
                                break;
                            case 6:
                                System.out.println("UPDATE CIRCUIT");
                                System.out.println("Choose Circuit's Name");
                                String circuitNameToUpdate = scC.nextLine();
                                System.out.println("Choose Circuit's Country");
                                String newCountry = scC.nextLine();
                                System.out.println("Choose Circuit's Km");
                                int newKm = scN.nextInt();
                                System.out.println("Choose Circuit's Race Time");
                                String newRaceTime = scC.nextLine();
                                boolean updateResult = circuitsController.update_by_nombre(circuitNameToUpdate, newCountry, newKm, newRaceTime);
                                System.out.println("Circuit updated: " + updateResult);
                                break;
                            case 7:
                                System.out.println("UPDATE KM");
                                System.out.println("Choose Circuit's Name");
                                String newCircuitsNameToUpdate = scC.nextLine();
                                System.out.println("Choose Circuit's Km");
                                int newCircuitKmToUpdate = scN.nextInt();
                                boolean updateResultTitles = circuitsController.update_km_by_nombre(newCircuitsNameToUpdate, newCircuitKmToUpdate);
                                System.out.println("Circuit updated: " + updateResultTitles);
                                break;
                            case 8:
                                break;
                            default:
                                System.out.println("Non valid option");
                                break;
                        }
                        break;
                    case 4:
                        break;
                    default:
                        System.out.println("Non valid option");
                }
            }while(option != 4);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");
    }

    public static void menu() {
        System.out.println("Choose Table");
        System.out.println("1. DRIVERS");
        System.out.println("2. TEAMS");
        System.out.println("3. CIRCUITS");
        System.out.println("4. Close");
    }

    public static void driversMenu() {
        System.out.println("1. SELECT_ALL");
        System.out.println("2. SELECT_BY_NUM");
        System.out.println("3. SELECT_AVG_AGE");
        System.out.println("4. INSERT_DRIVER");
        System.out.println("5. DELETE_DRIVER_BY_NUM");
        System.out.println("6. UPDATE_BY_NUM");
        System.out.println("7. UPDATE_CHAMPIONSHIPS_BY_NUM");
        System.out.println("8. EXIT");
    }

    public static void teamsMenu() {
        System.out.println("1. SELECT_ALL");
        System.out.println("2. SELECT_BY_NOMBRE");
        System.out.println("3. SELECT_AVG_TITLES");
        System.out.println("4. INSERT_TEAM");
        System.out.println("5. DELETE_TEAM_BY_NOMBRE");
        System.out.println("6. UPDATE_BY_NOMBRE");
        System.out.println("7. UPDATE_TITLES_BY_NOMBRE");
        System.out.println("8. EXIT");
    }

    public static void circuitsMenu() {
        System.out.println("1. SELECT_ALL");
        System.out.println("2. SELECT_BY_NOMBRE");
        System.out.println("3. SELECT_AVG_KM");
        System.out.println("4. INSERT_CIRCUIT");
        System.out.println("5. DELETE_CIRCUIT_BY_NOMBRE");
        System.out.println("6. UPDATE_BY_NOMBRE");
        System.out.println("7. UPDATE_KM_BY_NOMBRE");
        System.out.println("8. EXIT");
    }
}
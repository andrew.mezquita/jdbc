package controlador;

import modelo.Circuits;
import modelo.CircuitsDAO;
import modelo.Teams;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CircuitsJDBC implements CircuitsDAO {
    public final String SELECT_ALL = "select * from circuits";
    //b.
    public final String SELECT_BY_NOMBRE = "select * from circuits where nombre = ?";
    //c.
    public final String SELECT_AVG_KM = "select avg(km) as km from circuits";
    //d.
    public final String INSERT_CIRCUIT = "insert circuits values(?,?,?,?)";
    //e.
    public final String DELETE_CIRCUIT_BY_NOMBRE = "delete from circuits where nombre = ?";
    //f.
    public final String UPDATE_BY_NOMBRE = "update circuits set country = ?, km = ?, race_time = ? where nombre = ?";
    //g.
    public final String UPDATE_KM_BY_NOMBRE = "update circuits set km = ? where nombre = ?";

    Connection connection;

    public CircuitsJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ArrayList<Circuits> select_all() {
        ArrayList<Circuits> lista_circuits = new ArrayList<Circuits>();
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String country = rs.getString("country");
                int km = rs.getInt("km");
                String race_time = rs.getString("race_time");

                Circuits circuit = new Circuits(nombre, country, km, race_time);
                lista_circuits.add(circuit);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_circuits;
    }

    @Override
    public Circuits select_by_nombre(String nombre) {
        Circuits circuit = null;
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_BY_NOMBRE);
            stmt.setString(1,nombre);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String circuitNombre = rs.getString("nombre");
                String country = rs.getString("country");
                int km = rs.getInt("km");
                String race_time = rs.getString("race_time");

                circuit = new Circuits(nombre, country, km, race_time);
            } else {
                circuit = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return circuit;
    }

    @Override
    public double select_avg_km() {
        double avgKm = 0;
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_AVG_KM);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                avgKm = rs.getDouble("km");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return avgKm;
    }

    @Override
    public boolean insert_circuit(Circuits cicuit) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(INSERT_CIRCUIT);
            stmt.setString(1, cicuit.getNombre());
            stmt.setString(2, cicuit.getCountry());
            stmt.setInt(3, cicuit.getKm());
            stmt.setString(4, cicuit.getRace_time());

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean delete_circuit_by_nombre(String nombre) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(DELETE_CIRCUIT_BY_NOMBRE);
            stmt.setString(1, nombre);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }

    @Override
    public boolean update_by_nombre(String nombre, String country, int km, String race_time) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(UPDATE_BY_NOMBRE);
            stmt.setString(1, country);
            stmt.setInt(2, km);
            stmt.setString(3, race_time);
            stmt.setString(4, nombre);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean update_km_by_nombre(String nombre, int km) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(UPDATE_KM_BY_NOMBRE);
            stmt.setInt(1, km);
            stmt.setString(2, nombre);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }
}

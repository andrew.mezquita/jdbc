package controlador;

import modelo.Drivers;
import modelo.DriversDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DriversJDBC implements DriversDAO {
    //a.
    public final String SELECT_ALL = "select * from drivers";
    //b.
    public final String SELECT_BY_NUM = "select * from drivers where num = ?";
    //c.
    public final String SELECT_AVG_AGE = "select avg(age) as age from drivers";
    //d.
    public final String INSERT_DRIVER = "insert drivers values(?,?,?,?,?,?,?)";
    //e.
    public final String DELETE_DRIVER_BY_NUM = "delete from drivers where num = ?";
    //f.
    public final String UPDATE_BY_NUM = "update drivers set nombre = ?, surname = ?, age = ?, championships = ?, victories = ?, podiums = ? where num = ?";
    //g.
    public final String UPDATE_CHAMPIONSHIPS_BY_NUM = "update drivers set championships = ? where num = ?";

    Connection connection;

    public DriversJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ArrayList<Drivers> select_all() {
        ArrayList<Drivers> lista_drivers = new ArrayList<Drivers>();
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int num = rs.getInt("num");
                String nombre = rs.getString("nombre");
                String surname = rs.getString("surname");
                int age = rs.getInt("age");
                int championships = rs.getInt("championships");
                int victories = rs.getInt("victories");
                int podiums = rs.getInt("podiums");

                Drivers driver = new Drivers(num, nombre, surname, age, championships, victories, podiums);
                lista_drivers.add(driver);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_drivers;
    }

    @Override
    public Drivers select_by_num(int num) {
        Drivers driver = null;
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_BY_NUM);
            stmt.setInt(1,num);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int drivernum = rs.getInt("num");
                String nombre = rs.getString("nombre");
                String surname = rs.getString("surname");
                int age = rs.getInt("age");
                int championships = rs.getInt("championships");
                int victories = rs.getInt("victories");
                int podiums = rs.getInt("podiums");

                driver = new Drivers(drivernum, nombre, surname, age, championships, victories, podiums);
            } else {
                driver = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return driver;
    }

    @Override
    public double select_avg_age() {
        double avgAge = 0;
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_AVG_AGE);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                avgAge = rs.getDouble("age");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return avgAge;
    }

    @Override
    public boolean insert_driver(Drivers driver) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(INSERT_DRIVER);
            stmt.setInt(1, driver.getNumber());
            stmt.setString(2, driver.getName());
            stmt.setString(3, driver.getSurname());
            stmt.setInt(4, driver.getAge());
            stmt.setInt(5, driver.getChampionships());
            stmt.setInt(6, driver.getVictories());
            stmt.setInt(7, driver.getPodiums());

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean delete_driver_by_num(int num) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(DELETE_DRIVER_BY_NUM);
            stmt.setInt(1, num);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }

    @Override
    public boolean update_by_num(int num, String nombre, String surname, int age, int championships, int victories, int podiums) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(UPDATE_BY_NUM);
            stmt.setString(1, nombre);
            stmt.setString(2, surname);
            stmt.setInt(3, age);
            stmt.setInt(4, championships);
            stmt.setInt(5, victories);
            stmt.setInt(6, podiums);
            stmt.setInt(7, num);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean update_championships_by_num(int num, int championships) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(UPDATE_CHAMPIONSHIPS_BY_NUM);
            stmt.setInt(1, championships);
            stmt.setInt(2, num);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }
}

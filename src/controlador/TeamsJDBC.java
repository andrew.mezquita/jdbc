package controlador;

import modelo.Drivers;
import modelo.Teams;
import modelo.TeamsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TeamsJDBC implements TeamsDAO {
    //a.
    public final String SELECT_ALL = "select * from teams";
    //b.
    public final String SELECT_BY_NOMBRE = "select * from teams where nombre = ?";
    //c.
    public final String SELECT_AVG_TITLES = "select avg(titles) as titles from teams";
    //d.
    public final String INSERT_TEAM = "insert teams values(?,?,?,?,?,?)";
    //e.
    public final String DELETE_TEAM_BY_NOMBRE = "delete from teams where nombre = ?";
    //f.
    public final String UPDATE_BY_NOMBRE = "update teams set country = ?, director = ?, color = ?, power_unit = ?, titles = ? where nombre = ?";
    //g.
    public final String UPDATE_TITLES_BY_NOMBRE = "update teams set titles = ? where nombre = ?";

    Connection connection;

    public TeamsJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ArrayList<Teams> select_all() {
        ArrayList<Teams> lista_teams = new ArrayList<Teams>();
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String country = rs.getString("country");
                String director = rs.getString("director");
                String color = rs.getString("color");
                String power_unit = rs.getString("power_unit");
                int titles = rs.getInt("titles");

                Teams team = new Teams(nombre, country, director, color, power_unit, titles);
                lista_teams.add(team);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_teams;
    }

    @Override
    public Teams select_by_nombre(String nombre) {
        Teams team = null;
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_BY_NOMBRE);
            stmt.setString(1,nombre);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String teamNombre = rs.getString("nombre");
                String country = rs.getString("country");
                String director = rs.getString("director");
                String color = rs.getString("color");
                String power_unit = rs.getString("power_unit");
                int titles = rs.getInt("titles");

                team = new Teams(nombre, country, director, color, power_unit, titles);
            } else {
                team = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return team;
    }

    @Override
    public double select_avg_titles() {
        double avgTitles = 0;
        try {
            PreparedStatement stmt = connection.prepareStatement(SELECT_AVG_TITLES);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                avgTitles = rs.getDouble("titles");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return avgTitles;
    }

    @Override
    public boolean insert_team(Teams team) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(INSERT_TEAM);
            stmt.setString(1, team.getNombre());
            stmt.setString(2, team.getCountry());
            stmt.setString(3, team.getDirector());
            stmt.setString(4, team.getColor());
            stmt.setString(5, team.getPower_unit());
            stmt.setInt(6, team.getTitle());

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean delete_team_by_nombre(String nombre) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(DELETE_TEAM_BY_NOMBRE);
            stmt.setString(1, nombre);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;
    }

    @Override
    public boolean update_by_nombre(String nombre, String country, String director, String color, String power_unit, int titles) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(UPDATE_BY_NOMBRE);
            stmt.setString(1, country);
            stmt.setString(2, director);
            stmt.setString(3, color);
            stmt.setString(4, power_unit);
            stmt.setInt(5, titles);
            stmt.setString(6, nombre);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean update_titles_by_nombre(String nombre, int titles) {
        boolean salida = true;
        try {
            PreparedStatement stmt = connection.prepareStatement(UPDATE_TITLES_BY_NOMBRE);
            stmt.setInt(1, titles);
            stmt.setString(2, nombre);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }
}

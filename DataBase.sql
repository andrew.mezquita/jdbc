create database f1;
use f1;

drop table drivers;
create table drivers(
num int primary key unique,
nombre varchar(255),
surname varchar(255),
age int,
championships int,
victories int,
podiums int
);

insert drivers (num, nombre, surname, age, championships, victories, podiums) values
(14, "Fernando", "Alonso", 42, 2, 32, 106),
(33, "Max", "Verstappen", 27, 3, 56, 100),
(44, "Lewis", "Hamilton", 39, 7, 103, 196);

drop table teams;
create table teams(
nombre varchar(255) primary key unique,
country varchar(255),
director varchar(255),
color varchar(255),
power_unit varchar(255),
titles int
);

insert teams (nombre, country, director, color, power_unit, titles) values
("Ferrari", "Italy", "Vasseur", "Red", "Ferrari", 16),
("Mercedes", "Germany", "Wolf", "Grey", "Mercedes", 8),
("Red Bull", "Austria", "Horner", "Blue", "Honda", 7);

drop table circuits;
create table circuits(
nombre varchar(255) primary key unique,
country varchar(255),
km int,
race_time varchar(255)
);

insert circuits (nombre, country, km, race_time) values
("Spa", "Belgium", 7, "day"),
("Montmeló", "Spain", 9, "day"),
("Bahrain", "Bahrain", 5, "night");